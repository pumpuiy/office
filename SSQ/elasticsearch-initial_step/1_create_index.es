PUT /ssquare_analytic_accumulate_responsetime
{
  "mappings": {
      "properties": {
        "acc_total_request": {
          "type": "long"
        },
        "acc_total_request_1sec": {
          "type": "long"
        },
        "acc_total_request_3sec": {
          "type": "long"
        },
        "acc_total_request_5sec": {
          "type": "long"
        },
        "acc_total_request_10sec": {
          "type": "long"
        },
        "acc_total_request_15sec": {
          "type": "long"
        },
        "acc_total_request_30sec": {
          "type": "long"
        },
        "acc_total_request_gt30sec": {
          "type": "long"
        },
        "acc_total_request_lte1sec": {
          "type": "long"
        },
        "acc_total_request_lte3sec": {
          "type": "long"
        },
        "acc_total_request_lte5sec": {
          "type": "long"
        },
        "acc_total_request_lte10sec": {
          "type": "long"
        },
        "acc_total_request_lte15sec": {
          "type": "long"
        },
        "avg_response_time_sec": {
          "type": "float"
        },
        "sum_response_time_sec": {
          "type": "float"
        },
        "ssq_project_name": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 256
            }
          }
        },
        "ssq_server_ip": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 256
            }
          }
        },
        "ssq_service_name": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 256
            }
          }
        },
        "tx_access_time_minute": {
          "type": "date",
          "format": "YYYY-MM-DD HH:mm:ss||epoch_millis"
        }
      }
  }
}