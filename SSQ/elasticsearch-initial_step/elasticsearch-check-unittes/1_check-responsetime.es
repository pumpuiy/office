DELETE /ssquare_analytic_accumulate_responsetime_host1
DELETE /ssquare_analytic_accumulate_responsetime_host2

GET /ssquare_analytic_accumulate_responsetime_host2/_search
{
  "query": {
    "constant_score": {
      "filter": {
        "range": {
          "tx_access_time_minute": {
            "gt" : "now-1d/d",
            "lte" :  "now/d"
          }
        }
      }
    }
  }
}

GET /ssquare_analytic_accumulate_responsetime_host2/_search
{
  "query": {
    "constant_score": {
      "filter": {
        "range": {
          "tx_access_time_minute": {
            "gte": "2021-12-21||/d",
            "lte": "2021-12-21||/d",
            "format": "yyyy-MM-dd"
          }
        }
      }
    }
  }
}

GET /ssquare_analytic_accumulate_responsetime_host1/_search
{
  "query": {
    "constant_score": {
      "filter": {
        "range": {
          "tx_access_time_minute": {
            "gt" : "now-1d/d",
            "lte" :  "now/d"
          }
        }
      }
    }
  }
}

GET /ssquare_analytic_accumulate_responsetime_host1/_search
{
  "query": {
    "constant_score": {
      "filter": {
        "range": {
          "tx_access_time_minute": {
            "gte": "2021-12-21||/d",
            "lte": "2021-12-21||/d",
            "format": "yyyy-MM-dd"
          }
        }
      }
    }
  }
}

GET /ssquare_analytic_accumulate_responsetime_host1/_search
{
  "query": {
    "match_all": {}
  }
}


GET /ssquare_analytic_accumulate_responsetime_host1/_search
{
  "size": 1, 
  "query": {
      "bool": {
        "must": [
          {
            "match": {
              "ssq_service_name": "API_Delay10Sec"
            }
          }
        ]
      }
  },
  "aggs": {
    "total": { "sum": { "field": "acc_total_request" } }
  }
}


GET /ssquare_analytic_accumulate_responsetime_host2/_search
{
  "size": 1, 
  "query": {
      "bool": {
        "must": [
          {
            "match": {
              "ssq_service_name": "Demo_API_Delay2Sec"
            }
          }
        ],
        "filter": [
          {
            "range": {
              "tx_access_time_minute": {
                "gte": "2021-12-22||/d",
                "lte": "2021-12-22||/d",
                "format": "yyyy-MM-dd"
              }
            }
          }
        ]
      }
  },
  "aggs": {
    "total": { "sum": { "field": "acc_total_request" } }
  }
}