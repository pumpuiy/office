GET _cat/indices

DELETE /ssquare_analytic_accumulate_responsetime_host2

GET /ssquare_analytic_accumulate_responsetime_host1,ssquare_analytic_accumulate_responsetime_host2/_search
{
  "query": {
    "match": {
      "ssq_service_name.keyword": "API_Delay9Sec"
    }
  }
}

POST /ssquare_analytic_accumulate_responsetime_host2/_doc/
{
          "acc_total_request" : 1,
          "acc_total_request_10sec" : 0,
          "acc_total_request_15sec" : 0,
          "acc_total_request_1sec" : 0,
          "acc_total_request_3sec" : 1,
          "acc_total_request_5sec" : 0,
          "acc_total_request_lte10sec" : 1,
          "acc_total_request_lte15sec" : 1,
          "acc_total_request_lte1sec" : 0,
          "acc_total_request_lte3sec" : 1,
          "acc_total_request_lte5sec" : 1,
          "avg_response_time_sec" : 1.034000039100647,
          "ssq_project_name" : "DEMO_VM_6511_SYS",
          "ssq_server_ip" : "192.168.11.167",
          "ssq_service_name" : "API_Delay1Sec",
          "tx_access_time_minute" : 1630916640000
        }