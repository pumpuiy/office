GET /ssquare_analytic_accesslog_host1/_search
{
                      "size": 0,
                      "_source": [
                        "ssq_service_name",
                        "tx_usage_time_sec"
                      ],
                      "query": {
                        "constant_score": {
                          "filter": {
                            "range": {
                              "ssq_access_time": {
                                "from": "978310800000",
                                "to": "1637376600000"
                              }
                            }
                          }
                        }
                      },
                      "aggs": {
                        "my_buckets": {
                          "composite": {
                            "size": 200000,
                            "sources": [
                              {
                                "tx_access_time_minute": {
                                  "date_histogram": {
                                    "field": "ssq_access_time",
                                    "calendar_interval": "1m"
                                  }
                                }
                              },
                              {
                                "ssq_project_name": {
                                  "terms": {
                                    "field": "ssq_project_name.keyword"
                                  }
                                }
                              },
                              {
                                "ssq_service_name": {
                                  "terms": {
                                    "field": "ssq_service_name.keyword"
                                  }
                                }
                              },
                              {
                                "ssq_server_ip": {
                                  "terms": {
                                    "field": "ssq_server_ip.keyword"
                                  }
                                }
                              }
                            ]
                          },
                          "aggs": {
                            "buckets_response_time": {
                              "filters": {
                                "filters": {
                                  "acc_total_request_lte1sec": {
                                    "range": {
                                      "tx_usage_time_sec": {
                                        "lte": 1
                                      }
                                    }
                                  },
                                  "acc_total_request_lte3sec": {
                                    "range": {
                                      "tx_usage_time_sec": {
                                        "lte": 3
                                      }
                                    }
                                  },
                                  "acc_total_request_lte5sec": {
                                    "range": {
                                      "tx_usage_time_sec": {
                                        "lte": 5
                                      }
                                    }
                                  },
                                  "acc_total_request_lte10sec": {
                                    "range": {
                                      "tx_usage_time_sec": {
                                        "lte": 10
                                      }
                                    }
                                  },
                                  "acc_total_request_lte15sec": {
                                    "range": {
                                      "tx_usage_time_sec": {
                                        "lte": 15
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            "avg_response_time_sec": {
                              "avg": {
                                "field": "tx_usage_time_sec"
                              }
                            },
                            "sum_response_time_sec": {
                              "sum": {
                                "field": "tx_usage_time_sec"
                              }
                            },
                            "buckets_response_time2": {
                              "filters": {
                                "filters": {
                                  "acc_total_request_1sec": {
                                    "range": {
                                      "tx_usage_time_sec": {
                                        "gte": 0,
                                        "lte": 1
                                      }
                                    }
                                  },
                                  "acc_total_request_3sec": {
                                    "range": {
                                      "tx_usage_time_sec": {
                                        "gte": 1,
                                        "lte": 3
                                      }
                                    }
                                  },
                                  "acc_total_request_5sec": {
                                    "range": {
                                      "tx_usage_time_sec": {
                                        "gte": 3,
                                        "lte": 5
                                      }
                                    }
                                  },
                                  "acc_total_request_10sec": {
                                    "range": {
                                      "tx_usage_time_sec": {
                                        "gte": 5,
                                        "lte": 10
                                      }
                                    }
                                  },
                                  "acc_total_request_15sec": {
                                    "range": {
                                      "tx_usage_time_sec": {
                                        "gte": 10,
                                        "lte": 15
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }