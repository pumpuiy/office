GET /ssquare_analytic_accesslog_host2/_count

GET /ssquare_analytic_accesslog_host2/_search
{
  "size": 150, 
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "ssq_project_name": "SampleProject_ssq640mini"
          }
        },
        {
          "match": {
            "ssq_service_name": "Demo_Webservice"
          }
        },
        {
          "match": {
            "ssq_server_ip": "192.168.10.154"
          }
        },
        {
          "constant_score": {
            "filter": {
              "range": {
                "ssq_access_time": {
                  "from": "2021-10-20 04:02:00",
                  "to": "2021-10-20 04:02:59",
                  "format": "yyyy-MM-dd HH:mm:ss"
                }
              }
            }
          }
        }
      ]
    }
  }
}

GET /ssquare_analytic_accesslog_host2/_search
{
                      "size": 0,
                      "_source": [
                        "ssq_service_name",
                        "tx_usage_time_sec"
                      ],
                      "query": {
                        "constant_score": {
                          "filter": {
                            "range": {
                              "ssq_access_time": {
                                "from": "1634698200000",
                                "to": "1637738258000"
                              }
                            }
                          }
                        }
                      },
                      "aggs": {
                        "my_buckets": {
                          "composite": {
                            "size": 5, 
                            "sources": [
                              {
                                "tx_access_time_minute": {
                                  "date_histogram": {
                                    "field": "ssq_access_time",
                                    "calendar_interval": "1m"
                                  }
                                }
                              },
                              {
                                "ssq_project_name": {
                                  "terms": {
                                    "field": "ssq_project_name.keyword"
                                  }
                                }
                              },
                              {
                                "ssq_service_name": {
                                  "terms": {
                                    "field": "ssq_service_name.keyword"
                                  }
                                }
                              },
                              {
                                "ssq_server_ip": {
                                  "terms": {
                                    "field": "ssq_server_ip.keyword"
                                  }
                                }
                              }
                            ]
                          },
                          "aggs": {
                            "buckets_response_time": {
                              "filters": {
                                "filters": {
                                  "acc_total_request_lte1sec": {
                                    "range": {
                                      "tx_usage_time_sec": {
                                        "lte": 1
                                      }
                                    }
                                  },
                                  "acc_total_request_lte3sec": {
                                    "range": {
                                      "tx_usage_time_sec": {
                                        "lte": 3
                                      }
                                    }
                                  },
                                  "acc_total_request_lte5sec": {
                                    "range": {
                                      "tx_usage_time_sec": {
                                        "lte": 5
                                      }
                                    }
                                  },
                                  "acc_total_request_lte10sec": {
                                    "range": {
                                      "tx_usage_time_sec": {
                                        "lte": 10
                                      }
                                    }
                                  },
                                  "acc_total_request_lte15sec": {
                                    "range": {
                                      "tx_usage_time_sec": {
                                        "lte": 15
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            "avg_response_time_sec": {
                              "avg": {
                                "field": "tx_usage_time_sec"
                              }
                            },
                            "sum_response_time_sec": {
                              "sum": {
                                "field": "tx_usage_time_sec"
                              }
                            },
                            "buckets_response_time2": {
                              "filters": {
                                "filters": {
                                  "acc_total_request_1sec": {
                                    "range": {
                                      "tx_usage_time_sec": {
                                        "gte": 0,
                                        "lt": 1
                                      }
                                    }
                                  },
                                  "acc_total_request_3sec": {
                                    "range": {
                                      "tx_usage_time_sec": {
                                        "gte": 1,
                                        "lt": 3
                                      }
                                    }
                                  },
                                  "acc_total_request_5sec": {
                                    "range": {
                                      "tx_usage_time_sec": {
                                        "gte": 3,
                                        "lt": 5
                                      }
                                    }
                                  },
                                  "acc_total_request_10sec": {
                                    "range": {
                                      "tx_usage_time_sec": {
                                        "gte": 5,
                                        "lt": 10
                                      }
                                    }
                                  },
                                  "acc_total_request_15sec": {
                                    "range": {
                                      "tx_usage_time_sec": {
                                        "gte": 10,
                                        "lt": 15
                                      }
                                    }
                                  },
                                  "acc_total_request_30sec": {
                                    "range": {
                                      "tx_usage_time_sec": {
                                        "gte": 15,
                                        "lt": 30
                                      }
                                    }
                                  },
                                  "acc_total_request_more30sec": {
                                    "range": {
                                      "tx_usage_time_sec": {
                                        "gte": 30
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }