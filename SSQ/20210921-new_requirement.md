

aggregation : 1s, 2s, 3s

Requirement 

ใน 1 นาที มี project, service , request count , avg response time 

จำนวน transaction ที่ต่ำกว่า 1s, 3s, 5s, 10s



ex.
ใน 1 นาที มี service อะไรเข้ามาบ้าง

service		total access	avg response time		จำนวนครั้งที่ต่ำกว่า 1sec	จำนวนครั้งที่ต่ำกว่า 3sec	จำนวนครั้งที่ต่ำกว่า 5sec	จำนวนครั้งที่ต่ำกว่า 10sec
service A       2               2.5                     0                       2                   0                       0
service B       2               1.5                     1                       2                   0                       0        
service c       2               11                      0                       1                   1                       1 


Raw - Data
----------
time stamp  Service Name    response time(sec)
00:00:001   service A          3
00:00:002   service B          1
00:00:003   service C          2
00:00:004   service A          2
00:00:004   service B          2
00:00:005   service C          20