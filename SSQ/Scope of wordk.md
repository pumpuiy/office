####

zoom : 6818028974

Phase       Source      Target
-----       ------      ------
1           DB          Kafka   -> Elastic
2           Elastic     Elastic 
3           Elastic     Elastic .....
4           DB          Kafka   -> Elastic  

# BitBucket : https://bitbucket.beebuddy.net/ (narachai / 12341234)
----------------------------------------------------------------------------------------

# โปรแกรมของ Phase 1-2
# -----------------------
Project (System) : analytic-collector
Repository (Feature) : benthos-apig-accesslog-api   (Phase 1)
https://bitbucket.beebuddy.net/projects/ANC/repos/benthos-apig-accesslog-api/browse

Project (System) : analytic-collector
Repository (Feature) : benthos-apig-accesslog-aggregate-api   (Phase 2)
https://bitbucket.beebuddy.net/projects/anc/repos/benthos-apig-accesslog-aggregate-api/browse


# ข้อมูลจากตี้
# ------------
Remote VPN : forticlient
            Remote Gateway : 49.0.81.199 : 10443
            username : tb_narachai/nara@22465Kq

# SSQ - Analytic
# ----------------
16:18 Blueberry http://ssq-analytic-uat.beebuddy.net:9119/
16:18 Blueberry เอาไว้ stop / start stream node

16:18 Blueberry http://ssq-analytic-uat.beebuddy.net:4444/files/  (admin / apimlog)
16:19 Blueberry เอาไว้ deploy benthos ใน folder stream

elastic on web : https://ssq-analytic-uat.beebuddy.net/  (elastic / Asdf!234)

## Log ME-Gateway
http://ssq-analytic-uat.beebuddy.net:4444/files/me-gateway/logs/system/output.log

## postman test
run url test : http://ssq-analytic-uat.beebuddy.net:8123/me/gw/apis/stream.testing_pum/v1.0/run
log file : http://ssq-analytic-uat.beebuddy.net:4444/files/me-gateway/logs/streams/pum/

# ----------------------------------------------------------------------------------------

## *** Profile เครื่องใหม่
# SSQUARE APIM - stream
17:17 Blueberry http://192.168.11.67:4444/files/streams/ (admin / apimlog)

# MicroEngine Process Manager
17:17 Blueberry http://192.168.11.67:9119/

# Docker - Portainer 
17:17 Blueberry http://192.168.11.67:9999/portainer/#/auth (admin / password1234)
## กรณีมีการสร้าง stream ใหม่ (folder) จะต้องไป restart ที่
## Portainer > Containers > restart "me-gateway"

## postman test
run url test : http://192.168.11.67:8123/me/gw/apis/stream.testing_pum/v1.0/run

## ElasticSearch 
https://192.168.11.67/login?next=%2F (elastic / S3cur!ty)


## Kafka
address     : kafka1:29092
kafka_topic : "ssquare_analytic_accumulate_responsetime_host1"
client_id   : "ssquare_analytic_accumulate_responsetime_host1"

## Conduktor - kafka monitory
tunnel putty : login : consoleadmin/Zaq1Xsw2
port : 192.168.11.67:2181, 8083, 9092

### VDI : Load Test
IP: apigload.blueprint.co.th
User: apig01@blueprint.co.th
Password: apig01P@ssw0rd


# ----------------------------------------------------------------------------------------


# ME-Gateway02
------------
https://fb-storage.beebuddy.net/files/hyper-integration-dev/megw02/plugins/active/run/streams/


## Benthos Doc
https://graphql-faas.github.io/benthos/processors/



     
